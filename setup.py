import os
import codecs
from setuptools import setup, find_packages
import re

# Base directory of package
here = os.path.abspath(os.path.dirname(__file__))

# Long description
def read(*parts):
    with codecs.open(os.path.join(here, *parts), 'r') as fp:
        return fp.read()

# Extract version string
def version_string():
    with codecs.open(os.path.join(here, 'spikepost/__init__.py')) as fp:
            data = fp.read()
            version_match = re.search(r'version = \"(.*)\"', data)

    return version_match.group(1)

long_description = read('README.rst')
version = version_string()

setup(
        name="spikepost",
        version=version,
        description="Tool for post-processing riscv-isa-sim (spike) traces",
        long_description=long_description,
        classifiers=[
            "Programming Language :: Python :: 2.7",
            "Programming Language :: Python :: 3.6",
        ],
        author='Anmol Sahoo',
        author_email='anmol.sahoo25@gmail.com',
        license='BSD',
        packages=find_packages(),
        entry_points={
            "console_scripts": [
                "spikepost=spikepost:main",
            ],
        }
)

