import spikepost.parsers
from collections import Counter

'''Branch statistics of program'''                                   

help_message = '{:<20s} {:<10s}'.format('stat_branch', 'Statistics of backward jumps, forward jumps, unconditional jumps')

def compute(input_file, args):
    '''Compute the histogram'''
    total_insts = 0
    addr_min = None if args.addr_min is None else args.addr_min
    addr_max = None if args.addr_max is None else args.addr_max
    mode = args.mode
    totalforward_insts = 0
    totalBackward_insts = 0
    totalBranch_insts = 0
    totalUnconditional_insts = 0
    totalconditionalBranch_insts = 0
   
    with open(input_file) as fp:
        for line in fp: 
            addr = spikepost.parsers.extractAddress(line)

            if addr_min is not None and addr < addr_min:
                continue   
            if addr_max is not None and addr > addr_max:
                break

            inst = spikepost.parsers.parseInstruction(line, mode)

            if inst is None:
                print("Skipping", line)
                continue
            
            if (inst.instr_name == "beq" ) or \
            (inst.instr_name == "bne" ) or \
            (inst.instr_name == "blt" ) or \
            (inst.instr_name == "bltu" ) or \
            (inst.instr_name == "bge")  or \
            (inst.instr_name == "bgeu"):
            
                if inst.imm > 0:
                    totalforward_insts += 1
                elif inst.imm < 0:
                    totalBackward_insts += 1

            if (inst.instr_name == "jal" and inst.rd == ('x',0)) or \
            (inst.instr_name == "beq" and inst.rs1 == inst.rs2) or \
            (inst.instr_name == "bne" and inst.rs1 != inst.rs2):
               
                totalUnconditional_insts += 1
             

            if inst.instr_name is not None:
                total_insts += 1       
        totalconditionalBranch_insts = totalforward_insts + totalBackward_insts
        totalBranch_insts =  totalconditionalBranch_insts + totalUnconditional_insts
     
    print("Statistics of backward jumps, forward jumps, unconditional jumps\n")
    print("==================================================================")
    print("Total number of instructions: {0:d}\n".format(total_insts))
    print("Total number of forward instructions: {0:d}\n".format(totalforward_insts))
    print("Total number of backward instructions: {0:d}\n".format(totalBackward_insts))
    print("Total number of conditionalBranch instructions: {0:d}\n".format(totalconditionalBranch_insts))
    print("Total number of Unconditional instructions: {0:d}\n".format(totalUnconditional_insts))
    print("Total number of Branch instructions: {0:d}\n".format(totalBranch_insts)) 
